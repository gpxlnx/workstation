#!/bin/bash
sudo pacman -Syu # Para atualizar os repositórios padrão
sudo pacman -S install git curl ansible # Instale esses pacotes
ansible-galaxy collection install community.general # Instala collections que o playbook irá usar
ansible-galaxy install jahrik.yay
ansible-galaxy collection install kewlfft.aur
cd /tmp
git clone https://gitlab.com/davidpuziol/workstation.git
cd workstation/arch
ansible-playbook main.yaml --ask-become-pass -v # e digite a senha do seu usuáro